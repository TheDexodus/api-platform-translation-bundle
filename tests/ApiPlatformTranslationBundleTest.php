<?php

declare(strict_types=1);

namespace Tunet\ApiTranslationBundle\Tests;

use Tunet\ApiPlatformTranslationBundle\ApiPlatformTranslationBundle;
use PHPUnit\Framework\TestCase;

class ApiPlatformTranslationBundleTest extends TestCase
{
    public function testClassExist(): void
    {
        $this->assertTrue(class_exists(ApiPlatformTranslationBundle::class));
    }

    public function testExtensionIsLoaded(): void
    {
        $bundle = new ApiPlatformTranslationBundle();
        $this->assertNotNull($bundle->getContainerExtension());
    }
}
