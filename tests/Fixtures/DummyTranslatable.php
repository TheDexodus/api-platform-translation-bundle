<?php

declare(strict_types=1);

namespace Tunet\ApiPlatformTranslationBundle\Tests\Fixtures;

use Tunet\ApiPlatformTranslationBundle\Model\AbstractTranslatable;
use Tunet\ApiPlatformTranslationBundle\Model\TranslationInterface;

class DummyTranslatable extends AbstractTranslatable
{
    /**
     * {@inheritdoc}
     */
    protected function createTranslation(): TranslationInterface
    {
        return new DummyTranslation();
    }
}
