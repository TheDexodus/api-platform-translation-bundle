<?php

declare(strict_types=1);

namespace Tunet\ApiPlatformTranslationBundle\Tests\Fixtures;

use Tunet\ApiPlatformTranslationBundle\Model\AbstractTranslation;

class DummyTranslation extends AbstractTranslation
{
    /**
     * @var null|string
     */
    private $translation;

    /**
     * Set translation
     *
     * @param string $translation
     *
     * @return DummyTranslation
     */
    public function setTranslation(string $translation): DummyTranslation
    {
        $this->translation = $translation;

        return $this;
    }

    /**
     * Get translation
     *
     * @return null|string
     */
    public function getTranslation(): ?string
    {
        return $this->translation;
    }
}
