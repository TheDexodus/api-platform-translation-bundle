## Tunet Api Translation Bundle

### This is the fork: [Locastic Api Translation Bundle](https://github.com/Locastic/ApiPlatformTranslationBundle)

Translation bundle for [ApiPlatform](https://api-platform.com/) based on [Sylius translation](https://docs.sylius.com/en/1.2/book/architecture/translations.html)

Installation:
-------------
```
$ composer require tunet/api-platform-translation-bundle
```

Implementation:
--------------
**Translatable entity:**

- Extend your model/resource with `Tunet\ApiTranslationBundle\Model\AbstractTranslatable`
- Add `createTranslation()` method which returns new object of translation Entity. Example:
``` php
use Tunet\ApiPlatformTranslationBundle\Model\AbstractTranslatable;
use Tunet\ApiPlatformTranslationBundle\Model\TranslationInterface;

class Post extends AbstractTranslatable
{
    // ...
    
    protected function createTranslation(): TranslationInterface
    {
        return new PostTranslation();
    }
}
```

- Add a `translations`-property. Add the `translations` serializations group and make a connection to the translation entity:
``` php
use Tunet\ApiPlatformTranslationBundle\Model\AbstractTranslatable;

class Post extends AbstractTranslatable
{
    // ...
    
    /**
     * @ORM\OneToMany(targetEntity="PostTranslation", mappedBy="translatable", fetch="EXTRA_LAZY", indexBy="locale", cascade={"PERSIST"}, orphanRemoval=true)
     *
     * @Groups({"post_write", "translations"})
     */
    protected $translations;
}
```

- Add virtual fields for all translatable fields, and add read serialization group. 
Getters and setters must call getters and setters from translation class. Example:
``` php
use Tunet\ApiPlatformTranslationBundle\Model\AbstractTranslatable;
use Symfony\Component\Serializer\Annotation\Groups;

class Post extends AbstractTranslatable
{
    // ...
    
    /**
    * @Groups({"post_read"})
    */
    private $title;
    
    public function setTitle(string $title)
    {
        $this->getTranslation()->setTitle($title);
    }

    public function getTitle(): ?string
    {
        return $this->getTranslation()->getTitle();
    }
}
```


**Translation entity:**
- Add entity with all translatable fields. Name needs to be name of translatable entity + Translation
- Extend `Tunet\ApiPlatformTranslationBundle\Model\AbstractTranslation`
- Add serialization group `translations` to all fields and other read/write groups.
Example Translation entity:
``` php
use Symfony\Component\Serializer\Annotation\Groups;
use Tunet\ApiPlatformTranslationBundle\Model\AbstractTranslation;

class PostTranslation extends AbstractTranslation
{
    // ...

    /**
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="translations")
     */
    protected $translatable;
    
    /**
     * @ORM\Column(type="string")
     * 
     * @Groups({"post_read", "post_write", "translations"})
     */
    private $title;
    
    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"post_write", "translations"})
     */
    protected $locale;

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }
}
```


**Api resource**
- Add `translation.groups` filter if you would like to have option to return all translation objects in response.
If you don't use `translations` group, response will return only requested locale translation or fallback locale translation.
- Add translations to normalization_context for PUT and POST methods to make sure 
they return all translation objects.
- Example:
``` yaml
AppBundle\Entity\Post:
    itemOperations:
        get:
            method: GET
        put:
            method: PUT
            normalization_context:
                groups: ['translations']
    collectionOperations:
        get:
            method: GET
        post:
            method: POST
            normalization_context:
                groups: ['translations']
    attributes:
        filters: ['translation.groups']
        normalization_context:
            groups: ['post_read']
        denormalization_context:
            groups: ['post_write']
```

Usage:
------

**Language param for displaying single translation:** 

`?locale=de`

**Or use Accept-Language http header**

`Accept-Language: de`

**Serialization group for displaying all translations:** 

`?groups[]=translations`

**POST translations example**
``` json
{
    "datetime":"2017-10-10",
    "translations": { 
        "en":{
            "title":"test",
            "content":"test",
            "locale":"en"
        },
        "de":{
            "title":"test de",
            "content":"test de",
            "locale":"de"
        }
    }
}
```

**EDIT translations example**
``` json
{
    "datetime": "2017-10-10T00:00:00+02:00",
    "translations": {
        "de": {
          "id": 3,
          "title": "test edit de",
          "content": "test edit de",
          "locale": "de"
        },
        "en": {
          "id": 2,
          "title": "test edit",
          "content": "test edit",
          "locale": "en"
        }
    }
}
```
