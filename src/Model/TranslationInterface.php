<?php

declare(strict_types=1);

namespace Tunet\ApiPlatformTranslationBundle\Model;

interface TranslationInterface
{
    /**
     * @return TranslatableInterface
     */
    public function getTranslatable(): ?TranslatableInterface;

    /**
     * @param TranslatableInterface $translatable
     */
    public function setTranslatable(?TranslatableInterface $translatable): void;

    /**
     * @return null|string
     */
    public function getLocale(): ?string;

    /**
     * @param null|string $locale
     */
    public function setLocale(?string $locale): void;
}
