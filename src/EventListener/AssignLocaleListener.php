<?php

declare(strict_types=1);

namespace Tunet\ApiPlatformTranslationBundle\EventListener;

use Doctrine\Common\EventArgs;
use Tunet\ApiPlatformTranslationBundle\Model\TranslatableInterface;
use Tunet\ApiPlatformTranslationBundle\Translation\Translator;

class AssignLocaleListener
{
    /**
     * @var Translator
     */
    private $translator;

    /**
     * @var string
     */
    private $defaultLocale;

    public function __construct(Translator $translator, string $defaultLocale = 'en')
    {
        $this->translator = $translator;
        $this->defaultLocale = $defaultLocale;
    }

    public function postLoad(EventArgs $args): void
    {
        $this->assignLocale($args);
    }

    public function prePersist(EventArgs $args): void
    {
        $this->assignLocale($args);
    }

    private function assignLocale(EventArgs $args): void
    {
        $object = $args->getObject();

        if (!$object instanceof TranslatableInterface) {
            return;
        }

        $localeCode = $this->translator->loadCurrentLocale();

        $object->setCurrentLocale($localeCode);
        $object->setFallbackLocale($this->defaultLocale);
    }
}
